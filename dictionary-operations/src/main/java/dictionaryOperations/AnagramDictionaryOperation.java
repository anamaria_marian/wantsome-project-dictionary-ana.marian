package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class AnagramDictionaryOperation implements DictionaryOperation {
    private BufferedReader in;
    private Set<String> wordsNoDuplicates;
    public static Map<String, Set<String>> anagramsDictionary = new HashMap<>();

    public AnagramDictionaryOperation(Set<String> wordsNoDuplicates, BufferedReader in) {
        this.wordsNoDuplicates = wordsNoDuplicates;
        this.in = in;
    }

    private Set<String> getAnagrams(String sortedInput) {
        Set<String> anagramsList = new HashSet<>();
        for (String word : wordsNoDuplicates) {
            if (sortedInput.equals(Utils.returnAlphabeticLetters(word))) {
                anagramsList.add(word);
            }
        }
        return anagramsList;
    }

    @Override
    public void run() throws IOException {
        System.out.print("Insert a word: ");
        String userInput = in.readLine();
        System.out.println("From user: " + userInput);
        String sortedInput = Utils.returnAlphabeticLetters(userInput);
        if (anagramsDictionary.containsKey(sortedInput)) {
            anagramsDictionary.get(sortedInput).add(userInput);
        } else {
            Set<String> newSet = getAnagrams(sortedInput);// am folosit Set pt a-mi impiedica introducerea in value a duplicatelor (e.g. acia)

            newSet.add(userInput);

            anagramsDictionary.put(sortedInput, newSet);
        }

        System.out.println(anagramsDictionary.get(sortedInput));

    }


}
