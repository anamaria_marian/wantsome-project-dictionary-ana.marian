package dictionaryOperations;

import utils.Utils;

import java.io.IOException;
import java.util.Set;

//cauta toate palindroamele
public class PalindromDictionaryOperation implements DictionaryOperation {

    private Set<String> wordsSet;

    public PalindromDictionaryOperation(Set<String> wordsSet){
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Palindroms...");

       Set<String> palindroms = Utils.findAllPalindroms(wordsSet);

       for (String line: palindroms){
           System.out.println(line);
       }

        System.out.println("Done!");

    }
}
