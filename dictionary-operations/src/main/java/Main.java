import dictionaryOperations.AnagramDictionaryOperation;
import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.PalindromDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.ResourceInputFileReader;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("This is my main class.");
        // citim de la tastatura (urmatoarele 2 linii de cod)
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); //ofera posibilitatea de a citi de la tastatura
        //       String userInput = in.readLine(); // linia ptr a introduce textul



        List<String> words = new ArrayList<>(); // facem o lista
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect asa cum este definita in fileReader
        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru
        Set<String> wordsNoDuplicates = Utils.removedDuplicates(words);

        while (true) { //atat timp cat eu primesc input de la tastatura sa-mi primeasca de la tastaura
            System.out.println("Menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes \n" +
                    "3. Anagrams \n" +
                      "0. Exit");
            String userInput = in.readLine();
            if (userInput.equals("0")) {
                System.out.println("La revedere!");
                break;
            }
            //folosim switch pentru a naviga in meniu dupa ce am definit in SearchDictionaryOperation
            DictionaryOperation operation = null;
            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsNoDuplicates, in);
                    break;
                case "2":
                    operation = new PalindromDictionaryOperation(wordsNoDuplicates);
                    break;
                case "3":
                   operation = new AnagramDictionaryOperation(wordsNoDuplicates, in);
                   break;
                case "0":
                    System.out.println("Have a nice day! :)");
                    break;
                default:
                    System.out.println("Invalid option!");
            }
            if (operation != null) {
                operation.run();
            }
        }
//        System.out.println("User input: " + userInput);

//          // step 3 --- ca sa citim din fila introdusa (dex) si sa printam in consola
//        userInput = in.readLine();
//        System.out.println("Second input: " + userInput);
//        List<String> words = new ArrayList<>(); // facem o lista
//        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect
//        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru
//        for (String word : words) {
//            System.out.println(word);
//        }
//        Set<String> wordsNoDuplicates = utils.Utils.removeDuplicates(words);
//        for (String word : wordsNoDuplicates) {
//            System.out.println(word);
//        }
    }
}
