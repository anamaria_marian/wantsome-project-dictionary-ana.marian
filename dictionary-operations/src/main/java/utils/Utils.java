package utils;

import java.util.*;

public class Utils {

//set in care adaugam cuvintele pt a elimina duplicate
    public static Set<String> removedDuplicates(List<String> allLines){
        Set<String> wordsSet = new HashSet<>();

        for (String line: allLines){
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordsSet, String word){
        Set<String> result = new HashSet<>();
        for (String line:wordsSet){
            if(line.contains(word)){
                result.add(line);
            }
        }
        return result;
    }

    //e,g. Ana
    public static Set<String> findAllPalindroms(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reversedWord = new StringBuilder(line).reverse();
            if (line.equals(reversedWord.toString())) {
                result.add(line);
            }
        }

       return result;
    }


    //e.g. ana -- return: aan
    public static String returnAlphabeticLetters(String word) {
        char[] charArray = word.toLowerCase().toCharArray();
        Arrays.sort(charArray);
        String orderedLetters = new String(charArray);
        return orderedLetters;
    }

}
