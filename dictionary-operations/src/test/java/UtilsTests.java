import dictionaryOperations.AnagramDictionaryOperation;
import fileReader.ResourceInputFileReader;
import org.junit.Test;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UtilsTests {
    @Test
    public void testPalindromFinder(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("unu");
        Set<String> palindroms = Utils.findAllPalindroms(inputSet);/// --> palindromes has "unu"

        assertTrue(palindroms.contains("unu"));
    }

    @Test
    public void testPalindromesFromFile() throws IOException {
        List<String> words = new ArrayList<>(); // facem o lista
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader(); // creem un obiect
        words = resourceInputFileReader.readFile("dex.txt"); // asignam listei fisierul nostru
        Set<String> wordsNoDuplicates = Utils.removedDuplicates(words);

        Set<String> palindromes = Utils.findAllPalindroms(wordsNoDuplicates);

        assertTrue(palindromes.contains("abba"));
    }

    @Test
    public void testPalindromsNoResult(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("telefon");

        Set<String> palindromes = Utils.findAllPalindroms(inputSet);
        assertTrue(palindromes.size() == 0);
    }

    @Test
    public void testOrderedLetters(){
        //System.out.println(Utils.returnAlphabeticLetters("Bna"));
        assertEquals("aaci", Utils.returnAlphabeticLetters("Acia"));
    }

    @Test
    public void testAnagram() throws IOException{
//        Set<String> inputSet = new HashSet<>();
//        inputSet.add("acia");
//        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
//        AnagramDictionaryOperation anagrams = new AnagramDictionaryOperation(inputSet, in);
//        anagrams.run(); --- am vrut sa testez clasa AnagramDictionaryOperation
//
//        //Set<String> anagramsList = Utils.returnAlphabeticLetters("acia");



    }
}
